<?php

namespace Drupal\sitemaps_by_date\Form;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class SitemapSettingsForm extends FormBase {

  public function getFormId() {
    return 'sitemap_by_date_settings_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::config('sitemaps_by_date.settings');
    $cronInterval = $config->get('cron_interval');
    $maxExecutionTime = $config->get('max_execution_time');
    $bundles = $config->get('bundles');

    $form['cron_interval'] = [
      '#type' => 'number',
      '#min' => 0,
      '#required' => TRUE,
      '#title' => $this->t('Cron interval in seconds'),
      '#description' => $this->t('Throttle time until the next cron run.'),
      '#default_value' => isset($cronInterval) ? $cronInterval : 86400,
    ];
    $form['max_execution_time'] = [
      '#type' => 'number',
      '#min' => 1,
      '#max' => 180,
      '#required' => TRUE,
      '#title' => $this->t('Maximal execution time during cron runs - in seconds'),
      '#description' => $this->t('The maximum time the cron job can take while generating parts of the sitemaps.'),
      '#default_value' => isset($maxExecutionTime) ? $maxExecutionTime : 10,
    ];

    /** @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityBundleInfo */
    $entityBundleInfo = \Drupal::service('entity_type.bundle.info');
    $nodeBundles = $entityBundleInfo->getBundleInfo('node');
    $options = [];
    foreach ($nodeBundles as $bundleId => $nodeBundle) {
      $options[$bundleId] = $nodeBundle['label'];
    }

    $form['bundles'] = [
      '#type' => 'checkboxes',
      '#options' => $options,
      '#title' => $this->t('Bundles to put to sitemaps'),
      '#default_value' => isset($bundles) ? $bundles : [],
    ];

    /** @var \Drupal\Core\Queue\QueueFactory $queue_factory */
    $queue_factory = \Drupal::service('queue');
    $queue = $queue_factory->get('sitemaps_by_date');
    $numberOfItems = $queue->numberOfItems();

    $form['clear_queue'] = [
      '#type' => 'fieldset',
      '#prefix' => '<div id="ajax-count-wrapper">',
      '#suffix' => '</div>',
      '#weight' => -100,
      '#description' => $this->t('Current Queue Elements count: @count', ['@count' => $numberOfItems]),
    ];

    $form['clear_queue']['clear_queue_btn'] = [
      '#type' => 'submit',
      '#value' => 'Clear Queue',
      '#submit' => ['::emptyQueue'],
      '#ajax' => [
        'callback' => '::emptyQueueCallback',
        'wrapper' => 'ajax-count-wrapper',
      ],
    ];

    if ($numberOfItems === 0) {
      $form['clear_queue']['clear_queue_btn']['#disabled'] = TRUE;
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $cronInterval = Xss::filterAdmin($form_state->getValue('cron_interval'));
    $maxExecutionTime = Xss::filterAdmin($form_state->getValue('max_execution_time'));
    $bundles = $form_state->getValue('bundles');

    $config = \Drupal::configFactory()->getEditable('sitemaps_by_date.settings');
    $config->set('cron_interval', $cronInterval);
    $config->set('max_execution_time', $maxExecutionTime);
    $config->set('bundles', $bundles);
    $config->save();

    /** @var \Drupal\Core\Messenger\Messenger $messenger */
    $messenger = \Drupal::service('messenger');
    $messenger->addMessage($this->t('Saved.'));
  }

  public function emptyQueue(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Queue\QueueFactory $queue_factory */
    $queue_factory = \Drupal::service('queue');
    $queue = $queue_factory->get('sitemaps_by_date');
    $queue->deleteQueue();

    $itemsCount = $queue->numberOfItems();
    /** @var \Drupal\Core\Messenger\Messenger $messenger */
    $messenger = \Drupal::service('messenger');
    if ($itemsCount === 0) {
      $messenger->addMessage($this->t('Queue is now empty.'));
    }
    else {
      $messenger->addError($this->t('Queue cannot be emptied.'));
    }

    $form_state->setRebuild();
  }

  public function emptyQueueCallback(array &$form, FormStateInterface $form_state) {
    return $form['clear_queue'];
  }

}
