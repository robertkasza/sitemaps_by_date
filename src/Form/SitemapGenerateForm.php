<?php

namespace Drupal\sitemaps_by_date\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class SitemapGenerateForm extends FormBase {

  public function getFormId() {
    return 'sitemap_by_date_generate_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $currentYear = date('Y');
    $currentMonth = date('m');

    $form['from_year'] = [
      '#type' => 'number',
      '#min' => 1970,
      '#max' => 9999,
      '#required' => TRUE,
      '#title' => $this->t('From Year'),
      '#default_value' => $currentYear,
    ];
    $form['from_month'] = [
      '#type' => 'number',
      '#min' => 1,
      '#max' => 12,
      '#required' => TRUE,
      '#title' => $this->t('From Month'),
      '#default_value' => $currentMonth,
    ];
    $form['to_year'] = [
      '#type' => 'number',
      '#min' => 1970,
      '#max' => 9999,
      '#required' => TRUE,
      '#title' => $this->t('To Year'),
      '#default_value' => $currentYear,
    ];
    $form['to_month'] = [
      '#type' => 'number',
      '#min' => 1,
      '#max' => 12,
      '#required' => TRUE,
      '#title' => $this->t('To Month'),
      '#default_value' => $currentMonth,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create sitemap'),
    ];

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    $fromYear = $form_state->getValue('from_year');
    $fromMonth = $form_state->getValue('from_month');
    $toYear = $form_state->getValue('to_year');
    $toMonth = $form_state->getValue('to_month');

    if (empty($fromYear) && empty($fromMonth) && empty($toYear) && empty($toMonth)) {
      $form_state->setErrorByName('from_year', $this->t('All fields are required.'));
    }

    if ($fromYear > $toYear) {
      $form_state->setErrorByName('from_year', $this->t('From Year cannot be higher than the To Year.'));
    }

    if ($fromYear === $toYear && $fromMonth > $toMonth) {
      $form_state->setErrorByName('from_month', $this->t('From Month cannot be higher than the To month if they are in the same year.'));
    }

  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $fromYear = $form_state->getValue('from_year');
    $fromMonth = $form_state->getValue('from_month');
    $toYear = $form_state->getValue('to_year');
    $toMonth = $form_state->getValue('to_month');

    // Batch operation for each month.
    $batch = [
      'title' => $this->t('Create sitemaps from %from to %to', [
        '%from' => $fromYear . '-' . $fromMonth,
        '%to' => $toYear . '-' . $toMonth,
      ]),
      'finished' => 'Drupal\sitemaps_by_date\Form\SitemapGenerateForm::batchFinished',
      'operations' => [],
    ];

    for ($year = $fromYear; $year <= $toYear; $year++) {
      if ($fromYear === $toYear) {
        $monthStarting = $fromMonth;
        $monthEnding = $toMonth;
      }
      elseif ($year === $fromYear) {
        $monthStarting = $fromMonth;
        $monthEnding = 12;
      }
      elseif ($year === $toYear) {
        $monthStarting = 1;
        $monthEnding = $toMonth;
      }
      else {
        $monthStarting = 1;
        $monthEnding = 12;
      }

      for ($month = $monthStarting; $month <= $monthEnding; $month++) {
        $batch['operations'][] = [
          'Drupal\sitemaps_by_date\Controller\SitemapGenerate::updateSitemap',
          [$year, $month],
        ];
      }
    }

    batch_set($batch);
  }

  public static function batchFinished($success, $results, $operations) {
    $messenger = \Drupal::messenger();

    if ($success) {
      $message = t('Sitemap generating finished.');
      $messenger->addMessage($message);
    }
    else {
      $messenger->addError('There were errors during the sitemap generating process.');
    }
  }

}
