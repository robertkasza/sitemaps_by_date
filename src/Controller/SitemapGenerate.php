<?php

namespace Drupal\sitemaps_by_date\Controller;

use Drupal\Component\Utility\Timer;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\Core\Url;

class SitemapGenerate extends ControllerBase {

  public function generateUrlsForIds($ids, $entityType, $priority, $changeFreq) {
    $storage = \Drupal::entityTypeManager()->getStorage($entityType);
    $contents = $storage->loadMultiple($ids);
    $lastModFieldName = NULL;
    $result = [];
    foreach ($contents as $content) {
      $url = $content->toUrl('canonical', ['absolute' => TRUE])->toString();

      if ($content->hasField('changed')) {
        $lastModFieldName = 'changed';
      }
      elseif ($content->hasField('created')) {
        $lastModFieldName = 'created';
      }
      else {
        $lastModFieldName = NULL;
      }

      $lastMod = NULL;

      if (isset($lastModFieldName)) {
        $lastModFieldValue = $content->get($lastModFieldName)->getValue();
        if (isset($lastModFieldValue[0]['value'])) {
          $lastMod = gmdate(DATE_W3C, $lastModFieldValue[0]['value']);
        }

      }

      $result[] = [
        'loc' => $url,
        'lastmod' => $lastMod,
        'changefreq' => $changeFreq,
        'priority' => $priority,
      ];
    }
    return $result;
  }

  public static function getContentCount($entityType, $bundles, $startingDatetime, $endingDatetime) {
    $storage = \Drupal::entityTypeManager()->getStorage($entityType);
    $entityType = $storage->getEntityType();
    $statusFieldName = $entityType->getKey('status');
    $bundleFieldName = $entityType->getKey('bundle');
    $query = $storage->getQuery('AND');
    $query->condition($statusFieldName, 1);
    $query->condition($bundleFieldName, $bundles, 'IN');
    $group = $query->orConditionGroup()
      ->condition('field_domain_access', 'ma7_sk')
      ->condition('field_domain_all_affiliates', TRUE);
    $query->condition($group);
    $query->accessCheck(FALSE);
    $query->condition('created', $startingDatetime, '>=');
    $query->condition('created', $endingDatetime, '<=');
    return $query->count()->execute();
  }

  public static function nextContentIds($limit, $entityType, $bundles, $contentId, $startingDatetime, $endingDatetime) {
    $storage = \Drupal::entityTypeManager()->getStorage($entityType);
    $entityType = $storage->getEntityType();
    $idFieldName = $entityType->getKey('id');
    $statusFieldName = $entityType->getKey('status');
    $bundleFieldName = $entityType->getKey('bundle');
    $query = $storage->getQuery('AND');
    $query->condition($idFieldName, $contentId, '>');
    $query->condition($bundleFieldName, $bundles, 'IN');
    $query->sort($idFieldName, 'ASC');
    $group = $query->orConditionGroup()
      ->condition('field_domain_access', 'ma7_sk')
      ->condition('field_domain_all_affiliates', TRUE);
    $query->condition($group);
    $query->accessCheck(FALSE);
    $query->condition('created', $startingDatetime, '>=');
    $query->condition($statusFieldName, 1);
    $query->condition('created', $endingDatetime, '<=');
    $query->range(0, $limit);
    return $query->execute();
  }

  public static function updateSitemap($year, $month, &$context) {
    $entityType = 'node';
    if ((int) $month < 10) {
      $month = '0' . (int) $month;
    }
    $startingDatetime = strtotime("{$year}-{$month}-01 00:00:00 GMT+1");
    $endingDatetime = strtotime("{$year}-{$month}-01 00:00:00 GMT+1 +1 month") - 1;

    if (empty($context['sandbox'])) {
      $context['sandbox'] = [];
      $context['sandbox']['max'] = 0;
      $bundles = self::getNodeBundles();

      if (empty($bundles)) {
        $context['finished'] = TRUE;
        return;
      }

      $context['sandbox']['bundles'] = $bundles;
      $context['sandbox']['max'] = self::getContentCount($entityType, $bundles, $startingDatetime, $endingDatetime);

      if ($context['sandbox']['max'] == 0) {
        $context['finished'] = TRUE;
        return;
      }

      $context['sandbox']['started'] = \Drupal::time()->getRequestTime();
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['chunk_progress'] = 0;
      $context['sandbox']['current_id'] = 0;
      $context['sandbox']['chunk'] = 1;
      $context['sandbox']['filename'] = self::getDestinationFileName($year, $month, $context['sandbox']['chunk'], $context['sandbox']['started']);
      $context['sandbox']['file_new'] = TRUE;
      $context['finished'] = FALSE;

    }

    $limit = 100;
    $priority = '0.5';
    $changeFreq = 'daily';

    $contentIdsToProcess = self::nextContentIds(
      $limit,
      $entityType,
      $context['sandbox']['bundles'],
      $context['sandbox']['current_id'],
      $startingDatetime,
      $endingDatetime
    );

    /** @var \Drupal\sitemaps_by_date\Controller\SitemapGenerate $sitemapGenerate */
    $sitemapGenerate = \Drupal::service('sitemaps_by_date.generate');

    if (!empty($contentIdsToProcess)) {
      $urlsArray = $sitemapGenerate->generateUrlsForIds(
        $contentIdsToProcess,
        $entityType,
        $priority,
        $changeFreq
      );
      $result = $sitemapGenerate->getUrlsTemplate($urlsArray);
      $id = max($contentIdsToProcess);

      $sitemapGenerate->saveUrlsToFile(
        $result,
        $context['sandbox']['filename'],
        $context['sandbox']['file_new']
      );

      $context['sandbox']['file_new'] = FALSE;

      // In case any of the entities fail to load, increase the progress by the
      // stub entity count.
      $context['sandbox']['progress'] += count($contentIdsToProcess);
      $context['sandbox']['chunk_progress'] += count($contentIdsToProcess);

      $context['sandbox']['current_id'] = $id;
      $context['message'] = t('Sitemap creating: @id / @maxId from @year-@month', [
        '@id' => $context['sandbox']['progress'],
        '@maxId' => $context['sandbox']['max'],
        '@year' => $year,
        '@month' => $month,
      ]);
    }

    $context['finished'] = ($context['sandbox']['progress'] >= $context['sandbox']['max']) || empty($contentIdsToProcess);

    $finishFile = FALSE;
    if ($context['finished'] || $context['sandbox']['chunk_progress'] >= 2000) {
      $finishFile = TRUE;
      $sitemapGenerate->saveUrlsToFile(
        '',
        $context['sandbox']['filename'],
        $context['sandbox']['file_new'],
        $finishFile
      );
      $context['sandbox']['new_file_list'][$year . $month][] = $context['sandbox']['filename'];
    }

    if (!$context['finished'] && $finishFile) {
      $context['sandbox']['chunk_progress'] = 0;
      $context['sandbox']['chunk'] += 1;
      $context['sandbox']['filename'] = self::getDestinationFileName($year, $month, $context['sandbox']['chunk'], $context['sandbox']['started']);
      $context['sandbox']['file_new'] = TRUE;
    }

    if ($context['finished']) {
      if (!empty($context['sandbox']['new_file_list'])) {
        $siteMapFiles = \Drupal::state()->get('sitemaps_by_date');

        foreach ($context['sandbox']['new_file_list'] as $date => $files) {
          $siteMapFiles[$date] = $files;
        }

        \Drupal::state()->set('sitemaps_by_date', $siteMapFiles);

        $sitemapGenerate->generateNewSitemapIndexFile($siteMapFiles);
      }
    }
  }

  public static function getDestinationFileName($year, $month, $chunk, $started = NULL) {
    if (!isset($started)) {
      $started = \Drupal::time()->getRequestTime();
    }
    $directory = 'public://sitemaps/';
    $filename = $year . $month . $chunk . '-' . $started . '.xml';
    /** @var \Drupal\Core\File\FileSystemInterface $fileSystem */
    $fileSystem = \Drupal::service('file_system');
    $fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
    return $fileSystem->createFilename($filename, $directory);
  }

  public function getUrlsTemplate($urls) {
    $results = '';
    $keys = ['lastmod', 'changefreq', 'priority'];

    foreach ($urls as $url) {
      if (!isset($url['loc'])) {
        continue;
      }

      $newUrl = '<loc>' . $url['loc'] . '</loc>';

      foreach ($keys as $key) {
        if (isset($url[$key])) {
          $newUrl .= '<' . $key . '>' . $url[$key] . '</' . $key . '>';
        }
      }

      $results .= '<url>' . $newUrl . '</url>';
    }
    return $results;
  }

  public function saveUrlsToFile($newTextContent, $destination, $isFileNew = FALSE, $finishFile = FALSE) {
    if ($isFileNew) {
      $newTextContent = $this->getXmlHeader() . $newTextContent;
    }

    if ($finishFile) {
      $newTextContent .= $this->getXmlFooter();
    }

    // Write the contents to the file,
    // using the FILE_APPEND flag to append the content to the end of the file
    // and the LOCK_EX flag to prevent anyone else writing to the file
    // at the same time.
    return file_put_contents($destination, $newTextContent, FILE_APPEND | LOCK_EX);
  }

  public function getXmlHeader() {
    return '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';
  }

  public function getXmlFooter() {
    return '</urlset>';
  }

  protected function generateNewSitemapIndexText($siteMapFiles = NULL) {
    if (empty($siteMapFiles)) {
      $siteMapFiles = \Drupal::state()->get('sitemaps_by_date');
    }

    $result = '<?xml version="1.0" encoding="UTF-8"?><sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

    /** @var \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler */
    $moduleHandler = \Drupal::moduleHandler();
    if ($moduleHandler->moduleExists('simple_sitemap')) {
      if ($this->isExistsSimpleSitemapGenerated()) {
        $result .= '<sitemap><loc>' . Url::fromRoute('simple_sitemap.sitemap_default', [], ['absolute' => TRUE])->toString() . '</loc></sitemap>';
      }
    }

    foreach ($siteMapFiles as $date => $siteMapFilesArray) {
      foreach ($siteMapFilesArray as $siteMapFile) {
        $result .= '<sitemap><loc>' . file_create_url($siteMapFile) . '</loc></sitemap>';
      }
    }

    return $result . '</sitemapindex>';
  }

  public function generateNewSitemapIndexFile($siteMapFiles = NULL) {
    $text = $this->generateNewSitemapIndexText($siteMapFiles);
    $directory = 'public://sitemaps/';
    /** @var \Drupal\Core\File\FileSystemInterface $fileSystem */
    $fileSystem = \Drupal::service('file_system');
    $fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
    $destination = $directory . 'sitemap.xml';
    $fileSystem->saveData($text, $destination, FileSystemInterface::EXISTS_REPLACE);
  }

  public function cronGenerate($maxExecutionTime = 10000) {
    /** @var \Drupal\Core\Queue\QueueFactory $queue_factory */
    $queue_factory = \Drupal::service('queue');
    $queue = $queue_factory->get('sitemaps_by_date');
    Timer::start('sitemaps_by_date_generator');

    while ($queue->numberOfItems() > 0) {
      $item = $queue->claimItem();

      if (empty($item)) {
        break;
      }

      try {
        $year = $item->data['year'];
        $month = $item->data['month'];
        $context = $item->data['context'];
        SitemapGenerate::updateSitemap($year, $month, $context);
        $queue->deleteItem($item);

        if (array_key_exists('finished', $context) && !$context['finished']) {
          $newItem = [
            'year' => $year,
            'month' => $month,
            'context' => $context,
          ];
          $queue->createItem($newItem);
        }
      }
      catch (SuspendQueueException $e) {
        $queue->releaseItem($item);
        break;
      }
      catch (\Exception $e) {
        watchdog_exception('sitemaps_by_date', $e);
      }

      if (!empty($maxExecutionTime) && Timer::read('sitemaps_by_date_generator') >= $maxExecutionTime) {
        break;
      }
    }

  }

  public static function getNodeBundles() {
    $bundles = \Drupal::config('sitemaps_by_date.settings')->get('bundles');

    // Kiszedjük a nem bejelölt bundle-ket.
    if (empty($bundles)) {
      $bundles = [];
    }
    else {
      foreach ($bundles as $i => $bundle) {
        if (empty($bundle)) {
          unset($bundles[$i]);
        }
      }
    }
    return $bundles;
  }

  protected function isExistsSimpleSitemapGenerated() {
    $result = \Drupal::database()
      ->select('simple_sitemap', 's')
      ->fields('s', ['id'])
      ->condition('s.status', 1)
      ->range(0, 1)
      ->execute()
      ->fetchField(0);
    return ($result !== FALSE);
  }

}
